#!/usr/bin/env python3
# This was partly pasted from the randomcolors.py, modified by me though.
import turtle
import random

def hexcon(num):
	key = "0123456789abcdef"
	h = ""
	h16 = int(num/16)
	h1 = num % 16
	h = key[h16]+ key[h1]
	return h
w = turtle.Screen()
w.clear()
w.bgcolor("#000000")
t = turtle.Turtle()
t.speed(-1)
t.width(10)
for n in range(0,1444,3):
	red = random.randint(0,255)
	green = random.randint(0,255)
	blue = random.randint(0,255)
	print(red,green,blue)
	rhex = hex(red)
	ghex = hex(green)
	bhex = hex(blue)
	print("hex ",rhex,ghex,bhex)
	rhex = hexcon(red); ghex = hexcon(green); bhex = hexcon(blue)
	print("hex ",rhex,ghex,bhex)
	hexcolor = "#"+rhex+ghex+bhex
	t.pencolor(hexcolor)
	if ( n % 9 == 0 ):
		t.pencolor("#ffff00")
	t.goto(0,200)
	t.forward(50)
	t.seth(n)
	
w.exitonclick()
