#!/usr/bin/env python3
import turtle
import random

def hexcon(num): #This is decimal to hex.
	key = "0123456789abcdef" # hex key
	h = ""
	h16 = int(num/16)
	h1 = num % 16
	h = key[h16]+ key[h1]
	return h

x = 0; y = 0
w = turtle.Screen()
w.clear()
w.bgcolor("#7D6868") #provides background colour.
t = turtle.Turtle()
t.width(5)
t.speed(-1)
for n in range(100,361, 3):
	red = random.randint(0,255)
	green = random.randint(0,255)
	blue = random.randint(0,255)
	print(red,green,blue)
	rhex = hex(red)
	ghex = hex(green)
	bhex = hex(blue)
	print("hex ",rhex,ghex,bhex)
	rhex = hexcon(red); ghex = hexcon(green); bhex = hexcon(blue)
	print("hex ",rhex,ghex,bhex)
	hexcolor = "#"+rhex+ghex+bhex
	t.pencolor(hexcolor)
	if ( n % 2 == 0 ):
		t.pencolor("#ffff00")
	t.goto(0,0)
	t.forward(100)
	t.seth(n)
	#theta = n * (3.14159265358979323846264338327950288419716939937510582097494459230781640628620899/180)			#angles work nearly always with radians, theta helps balance the lines out and prevent the growth of angles in a circle.

w.exitonclick()
